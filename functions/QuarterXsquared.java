package functions;

public class QuarterXsquared extends FuncContainer {
	
	public QuarterXsquared(char symb) {
		super(symb);
	    setSymbol(symb);
	}
	
	public int function(int x) {
		return (int) Math.round(0.25 * (x * x));
	}

}

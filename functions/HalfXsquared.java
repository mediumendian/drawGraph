package functions;

import functions.FuncContainer;

public class HalfXsquared extends FuncContainer {
	
	public HalfXsquared(char symb) {
		super(symb);
	    setSymbol(symb);
	}
	
	public int function(int x) {
		return (int) Math.round(0.5 * (x * x));
	}

}

package functions;

public class Log extends FuncContainer {
	
	public Log(char symb) {
		super(symb);
	    setSymbol(symb);
	}
	
	public int function(int x) {
		return -2 * (int) Math.round(Math.log(x));
	}
}

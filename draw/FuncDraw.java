package draw;

import java.util.Arrays;

public class FuncDraw {

	private functions.FuncContainer[] f;
	private final int WIDTH;
	private final int HEIGHT;
	private final char EMPTY_SYMB;
	private final int x_offset;

	private char[][] canvas;

	public FuncDraw(int width, char emptySymb,
			functions.FuncContainer... f) {
		this.WIDTH = width;
		this.f = f;
		this.HEIGHT = Math.abs(range(WIDTH)[1]) + range(WIDTH)[0] + 1;
		this.EMPTY_SYMB = emptySymb;
		this.canvas = new char[WIDTH][HEIGHT];
		this.x_offset = Math.abs(range(WIDTH)[1]);

		// System.out.println(range(WIDTH)[0] + " " + range(WIDTH)[1]);

		System.out.printf("Width will be %s and height will be %s.\n\n", WIDTH,
				HEIGHT);

		// fill all entries with EMPTY_SYMB
		for (int i = 0; i < canvas.length; i++) {
			Arrays.fill(canvas[i], EMPTY_SYMB);
		}

		canvas = drawXaxis(canvas);

		for (int n = 0; n < f.length; n++) {

			canvas = applyFunc(canvas, n, f[n].getSymbol());
		}

		canvas = transpose(canvas);

	}

	public int[] range(int WIDTH) {
		int maxVal = 0;
		int minVal = 0;

		for (int n = 0; n < f.length; n++) {
			for (int i = 1; i <= WIDTH; i++) {
				if (f[n].function(i) > maxVal) {
					maxVal = (int) Math.ceil(f[n].function(i));
				}
				if (f[n].function(i) < minVal) {
					minVal = (int) Math.floor(f[n].function(i));
				}
			}
		}
		return new int[] { maxVal, minVal };
	}

	public void print() {
		for (char[] sub : this.canvas) {
			for (char entry : sub) {
				System.out.print(entry + " ");
			}
			System.out.println();
		}

	}

	@SuppressWarnings("unused")
	private void print(char[] cs) {
		for (int i = 0; i < cs.length - 1; i++) {
			System.out.print(cs[i] + ", ");
		}
		System.out.println(cs[cs.length - 1]);
	}

	public void print(char[][] tab) {
		for (char[] sub : tab) {
			for (char entry : sub) {
				System.out.print(entry + " ");
			}
			System.out.println();
		}
	}

	private char[][] applyFunc(char[][] emptyCanv, int index, char GRAPH_SYMB) {
		for (int x = 0; x < emptyCanv.length; x++) {
			emptyCanv[x][x_offset + (int) Math.round(f[index].function(x))] = GRAPH_SYMB;
		}
		return emptyCanv;
	}

	private char[][] drawXaxis(char[][] emptyCanv) {
		for (int x = 0; x < emptyCanv.length; x++) {
			emptyCanv[x][x_offset] = '―';
		}
		return emptyCanv;
	}

	private char[][] transpose(char[][] arr) {
		char[][] transposed = new char[arr[0].length][arr.length];

		for (int i = 0; i < transposed.length; i++) {
			for (int j = 0; j < transposed[0].length; j++) {
				transposed[transposed.length - 1 - i][j] = arr[j][i];
			}
		}
		return transposed;
	}

}
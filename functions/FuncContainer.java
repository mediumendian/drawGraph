package functions;


public abstract class FuncContainer {

	protected char symbol;
	
	public FuncContainer(char symb) {
		symbol = symb;
	}

	protected void setSymbol(char symbol) {
		this.symbol = symbol;
	}

	public char getSymbol() {
		return symbol;
	}

	public int function(int x) {
		return x;
	}

}

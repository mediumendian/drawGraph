package draw;

import functions.*;

public class Control {
	public static void main(String[] args) {
		FuncContainer f = new Sin('+');
		FuncContainer g = new Mod6('½');
		FuncContainer h = new Log('*');
		FuncDraw fd = new FuncDraw(64, '´', f, g, h);
		fd.print();
	}
}

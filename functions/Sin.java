package functions;

public class Sin extends FuncContainer {
	
	public Sin(char symb) {
		super(symb);
	    setSymbol(symb);
	}
	
	public int function(int x) {
		return (int) (Math.round(Math.sin(0.1 * x) * 10));
	}
}

package functions;

public class Mod6 extends FuncContainer {

	public Mod6(char symb) {
		super(symb);
	    setSymbol(symb);
	}

	public int function(int x) {
		return x % 6;
	}

}
